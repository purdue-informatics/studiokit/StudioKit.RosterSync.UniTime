﻿using System;

namespace StudioKit.RosterSync.UniTime.Exceptions
{
	public class UniTimeDownException : Exception
	{
		public UniTimeDownException()
		{
		}

		public UniTimeDownException(string message)
			: base(message) { }

		public UniTimeDownException(string message, Exception inner)
			: base(message, inner) { }
	}
}