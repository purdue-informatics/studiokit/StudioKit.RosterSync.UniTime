﻿using StudioKit.RosterSync.UniTime.Models;
using System;
using System.Collections.Generic;

namespace StudioKit.RosterSync.UniTime.Classes
{
	public class EnrollmentComparer : IEqualityComparer<Enrollment>
	{
		public bool Equals(Enrollment x, Enrollment y) => x.ExternalId.Equals(y.ExternalId, StringComparison.OrdinalIgnoreCase);

		public int GetHashCode(Enrollment obj) => obj.ExternalId.GetHashCode();
	}
}