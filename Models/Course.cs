﻿using StudioKit.RosterSync.Interfaces;

namespace StudioKit.RosterSync.UniTime.Models
{
	public class Course : ICourse
	{
		public string SubjectArea { get; set; }
		public string CourseNumber { get; set; }
		public string CourseTitle { get; set; }
		public string ClassSuffix { get; set; }
	}
}