﻿namespace StudioKit.RosterSync.UniTime.Models
{
	public class Enrollment
	{
		public string StudentId { get; set; }
		public string ExternalId { get; set; } // Known as PUID
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
	}
}