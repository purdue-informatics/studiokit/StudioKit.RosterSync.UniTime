﻿using System;
using System.Collections.Generic;

namespace StudioKit.RosterSync.UniTime.Models
{
	public class Role
	{
		public RoleStatus Status { get; set; }
		public string Year { get; set; }
		public string Term { get; set; }
		public string Campus { get; set; }
		public List<string> Roles { get; set; }
		public DateTime EventBeginDate { get; set; }
		public DateTime EventEndDate { get; set; }
	}
}