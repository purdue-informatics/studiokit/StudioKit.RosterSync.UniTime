﻿using System.Text.RegularExpressions;

namespace StudioKit.RosterSync.UniTime.Utilities
{
	public static class Helpers
	{
		public static string GetTermCode(int year, string semester)
		{
			switch (semester)
			{
				case "Fall":
					return $"{year + 1}10";

				case "Spring":
					return $"{year}20";

				case "Summer":
					return $"{year}30";

				default:
					return $"{year}40";
			}
		}

		public static string GetTermReference(string termCode)
		{
			var match = Regex.Match(termCode, @"(\d{4})(\d{2})");
			var year = match.Groups[1].Value;
			var suffix = match.Groups[2].Value;
			var season = "";
			switch (suffix)
			{
				case "10":
					season = "Fall";
					break;

				case "20":
					season = "Spring";
					break;

				case "30":
					season = "Summer";
					break;
			}
			var yearInt = int.Parse(year);
			var adjustedYear = suffix == "10" ? yearInt - 1 : yearInt;
			return $"{season}{adjustedYear}";
		}

		public static string Base64Encode(string plainText)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
			return System.Convert.ToBase64String(plainTextBytes);
		}

		public static string Base64Decode(string base64EncodedData)
		{
			var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
			return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
		}
	}
}